# Microservice
##### Diklat PT Pupuk Sriwidjaja Palembang 

## Prerequisites:
- Python 2.7+
- VirtualEnv
- VirtualEnvWrapper (optional)
- MySQL Database

## Install
- Open Terminal/Windows Command Prompt
- Clone Repo: `git clone git@gitlab.com:erwin.yusrizal/microuser.git`
- `cd microuser`
- `virtualenv env`

## Activate VirtualEnv
- (Windows) `env\Scripts\activate`
- (Linux & MacOSX) `source env/bin/activate`
- `pip install -r requirements.txt`
- Note: if you have any issue during the extension/plugin installation, it might be caused by the plugins are not supported by your OS

## Configuration
- Open `microuser/config.py` file
- Fill `SERVER_NAME`
- Fill `CACHE_REDIS_URL` with your redis host and db
- Fill `RATELIMIT_STORAGE_URL` with your redis host and db
- Fill `CELERY_BROKER_URL` with your redis host and db
- Fill `CELERY_RESULT_BACKEND` with your redis host and db
- Fill `SQLALCHEMY_DATABASE_URI` with your MySQL DB host
- Fill all SMTP Email detail
- Save
- Open `api/endpoints/auth.py`
- Fill redis connection detail
- Save
- Open `microuser/manage.py` file
- Set host config (only for werkzeug)
- ``` manager.add_command('runserver', Server(
    threaded=True,
    use_reloader=True,
    use_debugger=True,
    host='api.ringsatu',
    port=3000))
- Note: host name and the port has to be same with your `SERVER_NAME` configuration

## Database Migration
- Open Terminal/Windows Command Prompt
- Go to working directory
- Activate VirtualEnv (if not done yet)
- Type: `python manage.py shell`
- Type: `db.create_all()`
- Press: CTRL + D to `exit`

## Run Server Werkzeug (Windows)
- Open Terminal/Windows Command Prompt
- Go to working directory
- Activate VirtualEnv (if not done yet)
- Type: `ptyhon manage.py runserver`
- Open browser, access host that you have set previously in config file `SERVER_NAME`

## Run Server Gunicorn + eventlet (Linux & MacOSX)
- Open Terminal/Windows Command Prompt
- Go to working directory
- Activate VirtualEnv (if not done yet)
- Type: `gunicorn -w 2 -b 0.0.0.0:3000 manage:app --reload`
- Open browser, access host that you have set previously in config file `SERVER_NAME`

## Run Celery Worker (Linux, MacOSX, WSL)
- Open Terminal/Windows Command Prompt
- Go to working directory
- Activate VirtualEnv (if not done yet)
- Type: `celery -A worker.celery worker --loglevel=info`

## Setup Celery Flower (Linux, MacOSX, WSL)
- Open Terminal/Windows Command Prompt
- Type: `mkdir celeryflower`
- Type: `cd celeryflower`
- Type: `virtualenv env`
- Activate VirtualEnv
- Type: `pip install redis celery flower`
- Type: `pip freeze > requirements.txt`
- Type: `celery flower --port=5555 --broker=redis://:password@host/db` ==> check your celery configuration in application config.py

## Test API
- Install Postman (if not done yet)
- Import collection `Microservice.postman_collection.json`

## Elasticsearch & Kibana
- Install JRE/JDK 8
- Install Elasticsearch 7.3, follow the installation instructions here https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started-install.html
- Install Kibana 7.3, follow the installation instructions here https://www.elastic.co/guide/en/kibana/current/install.html
- Config Elasticsearch & kibana, set the application name, host and port

## Elasticsearch & Kibana Troubleshooting
Since we're using the latest version of Elasticsearch and Kibana, its no longer support the _doc_type and also it has some issue with the permission. To solve it, you can do this request using Terminal/Windows Command Prompt

```
curl -XPUT -H "Content-Type: application/json" http://localhost:9200/_cluster/settings -d '{ "transient": { "cluster.routing.allocation.disk.threshold_enabled": false } }'
```

```
curl -XPUT -H "Content-Type: application/json" http://localhost:9200/_all/_settings -d '{"index.blocks.read_only_allow_delete": null}'
```

## HAPPY INDEXING!


