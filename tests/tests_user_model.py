import unittest

from api import create_app, db
from api.v1.models.users import (
    PermissionModel,
    RoleModel,
    UserModel
)
from api.v1.utils import generate_unique_id

class AuthTestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
    
    def tearDown(self):
        self.app_context.pop()
    
    def test_tambah(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])