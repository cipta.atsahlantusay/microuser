ADD=git add .
COMMIT=git commit -m "$(message)"
PUSH=git push origin master
STATUS=git status

push:
	$(ADD)
	$(COMMIT)
	$(PUSH)

status:
	$(STATUS)

