import json
import string
import random
import arrow
import base64

from flask import (
    request,
    current_app
)

from math import ceil
from wheezy.core.feistel import make_feistel_number
from wheezy.core.feistel import sample_f

feistel_number = make_feistel_number(sample_f)

def generate_sms_token(phone='', n=6):
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits + phone) for _ in range(n))

def generate_unique_id(n=16):
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits + str(arrow.utcnow().timestamp)) for _ in range(n))


def get_clients_ip():
    headers_list = request.headers.getlist("X-Forwarded-For")
    user_ip = headers_list[0] if headers_list else request.remote_addr
    return user_ip

def parse_verification_token(token):
    try:
        payload = json.loads(base64.urlsafe_b64decode(str(token)))
        return payload
    except ValueError:
        return None

def normalize_phone_number(phone_number):

    phone = phone_number

    if phone_number.startswith('08'):
        phone = phone_number.replace(phone_number[:2], '628')
    elif phone_number.startswith('+628'):
        phone = phone_number.replace(phone_number[:4], '628')
    
    return phone


def denormalize_phone_number(phone_number):

    phone = phone_number
    
    if phone_number.startswith('628'):
        phone = phone_number.replace(phone_number[:3], '08')
    elif phone_number.startswith('+628'):
        phone = phone_number.replace(phone_number[:4], '08')       
    
    return phone


def reverse_id(id):
    return feistel_number(id)
