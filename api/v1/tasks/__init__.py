
import random

from flask import current_app, render_template
from flask_mail import Message

from api import mail, celery

from api.v1.utils import (
    normalize_phone_number
)

def exponential_backoff(task_self):
    minutes = task_self.default_retry_delay / 60
    rand = random.uniform(minutes, minutes * 1.3)
    return int(rand ** task_self.request.retries) * 60

'''
Send Celery Email Task Handler
'''

@celery.task(name='Celery Micro User Email Task', bind=True, max_retries=5, acks_late=True, default_retry_delay=10)
def send_celery_email_queue(self, *args, **kwargs):

    message = args if args else kwargs

    if type(message) == tuple:
        message = message[0]

    email = Message(message["subject"], sender=("Micro.id", "robot@missana.id"), recipients=[message["to"]])
    email.body = render_template(message["template"] + '.txt', message=message["variables"])
    email.html = render_template(message["template"] + '.html', message=message["variables"])

    if 'attachment' in message['variables']:
        if message['variables']['attachment']:
            email.attach(message['variables']['attachment'], 'application/octect-stream', open(message['variables']['attachment'], 'rb').read())

    try:
        mail.send(email)
    except Exception as e:
        self.retry(exc=e, countdown=exponential_backoff(self))



