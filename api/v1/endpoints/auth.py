# -*- coding: utf-8 -*-

import arrow
import bleach
import redis
import time
import re

from ua_parser import user_agent_parser as ua_parser

from datetime import (
    datetime,
    timedelta
)

from flask import (
    g,
    current_app,
    jsonify,
    request,
    url_for
)
from flask_cors import cross_origin

from flask_restful import (
    Resource
)
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    parser
)

from api import cache, db, mail
from api.v1 import api
from api.v1.models.users import (
    UserModel,
    RoleModel
)
from api.v1.schemas.users import(
    UserSchema
)

from api.v1.utils import(
    reverse_id,
    parse_verification_token,
    get_clients_ip,
    normalize_phone_number,
    denormalize_phone_number,
)   
from api.v1.decorators import (
    anonymous_required,
    permission_required,
    current_user
)


class Login(Resource):
    
    '''
    @method POST
    @endpoint /v1/auth/login
    @permission anonymous
    @return verification_code, expiracy
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields.')  

    login_args = {
        'identity': fields.Str(required=True, validate=[validate_required]),
        'access': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['dashboard', 'mobile'], error="Invalid choice")
        ]) 
    }

    @cross_origin()
    @anonymous_required()
    @use_args(login_args, locations=['json'])
    def post(self, payload):

        ''' Login '''

        identity = payload['identity']
        access = payload['access']
        phone_match = re.search(r'\(?(?:\+628|628|08)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}', identity)

        if phone_match:
            phone = normalize_phone_number(identity)
            user = UserModel.get_by(phone=phone)
        else:
            user = None

        if not user:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'identity': ['Invalid phone number.']
                    }
                }
            }), 422

        elif not  user.check_permission(access, 'viewall'):

            return jsonify({
                'success': False,
                'errors': {
                    'code': 403,
                    'messages': {
                        'permission': ['You do not have permission to access this resource.']
                    }
                }
            }), 403

        elif user.status != 'verified':
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'status': ['Your account is inactive or has been banned by administrator.']
                    }
                }
            }), 422

        else:

            verification_code = user.verify()
            return jsonify({
                'success': True,
                'message': '',
                'data': {
                    'verification_code': verification_code
                }                
            }), 200


api.add_resource(Login, '/auth/login')

class Register(Resource):
    '''
    @method POST
    @endpoint /v1/auth/register
    @return 
    '''

    def validate_required(value):
    if len(value.strip()) == 0:
        raise ValidationError('Please fill all required fields')   

    def validate_phone(value):
        phone_match = re.search(r'\(?(?:\+628|628|08)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}', value)

        if phone_match is None:
            raise ValidationError('Invalid phone number')
    
    post_args = {
        'fullname': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9\s]{1,255})$', error='Panjang nama lengkap maksimal 255 karakter. Hanya huruf, angka, dan spasi yang diperbolehkan.')
        ]),
        'phone': fields.Str(required=True, validate=[validate_required,validate_phone]),
        'email': fields.Email(required=True, validate=[validate_required])
        'only': fields.DelimitedList(fields.Str(), missing=None)
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @use_args(post_args, locations=['json'])
    def post(self, payLoad):
        '''Register'''

        fullname = bleach.clean(payload['fullname']) 
        phone = normalize_phone_number(payload['phone'])
        email = bleach.clean(payLoad['email'].lower(), strip=True)

        user = UserModel.query.filter(or_(UserModel.phone==phone, UserModel.email==email)).first()

        if user:
            return make_response(jsonify({
                'success': False,
                'errors': {
                    'code': 409,
                    'messages': {
                        'identity': ['Another user with this number or email already exist.']
                    }
                }
            }), 409)

        user_id = uuid.uuid4()

        u = UserModel()
        u.id = user_id
        u.email = email
        u.phone = phone
        u.status = 'unverified'
        u.role_id = 'd7d56ea1-3cc1-42cc-87f5-b162c46f7225'

        db.session.add(u)

        profile_id = uuid.uuid4()

        profile = ProfileModel()
        profile.id = profile_id
        profile.fullname = fullname
        profile.user_id = user_id
        
        db.session.add(profile)

        #create kong consumer account

        consumer_resp = request.post(current_app.config.get('KONG_API_URL') + '/consumers', data={
            "username": email.
            "custome_id":str(user_id)
        })

        if not consumer_resp.ok:
            db.session.rollback()

            return make_response(jsonify({
                'success' : False,
                'errors' : {
                    'code': 422,
                    'messages': {
                        'register': ['Something went wrong. Please try agin later.']
                    }
                }
            }),422)

            consumer_json = consumer_resp.json()

            #assign jwt to new user
            consumer_jwt_resp = request.post(current_app.config.get('KONG_API_URL') + '/consumers/' + consumer_json['id'] + '/jwt')

            if not consumer_jwt_resp.ok:

                db.session.rollback()

                return make_response(jsonify({
                    'success' : False,
                    'errors' : {
                        'code': 422,
                        'messages': {
                            'register': ['Something went wrong. Please try agin later.']
                        }
                    } 
                }),422)

            consumer_jwt_json = consumer_jwt_resp.json()

            u.public_key = consumer_jwt_json['key']
            u.privae_key = consumer_jwt_json['secret']

            db.session.commit()

            #add acl group permissions
            permissions = u.user_role.permissions
            for module in permissions:
                for(key,value) in permissions[module].items():
                    if value > 0:
                        consumer_group_resp = request.post(current_app.get('KONG_API_URL') + '/consumers/' + consumer_json['id'] + '/acls', data={
                            'group' : module':'+key
                        })
            
            schema = UserSchema(only=payLoad['only'], exclude=payload['exclude'])
            user = schema.dump(u).data

            return make_response(jsonify({
                    'success' : True,
                    'messages': {
                        'register': ['Something went wrong. Please try agin later.']
                    }
                }),201)


api.add_resource(Verify, '/auth/register')

class Verify(Resource):
    
    '''
    @method POST
    @endpoint /v1/auth/verify
    @permission anonymous
    @return access_token
    '''

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError('Please fill all required fields')   

    verify_args = {
        'code': fields.Str(required=True, validate=[validate_required]),
        'passkey': fields.Str(required=True, validate=[validate_required]),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @anonymous_required()
    @use_args(verify_args, locations=['json'])
    def post(self, payload):

        ''' Verification '''

        code = payload['code']
        passkey = payload['passkey']

        data = parse_verification_token(code)

        list_keys = ['module', 'signature', 'expire']

        if not any(p in list_keys for p in data) or (data['expire'] <= int(time.time())):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'verification_code': ['Invalid verification code or has expired.']
                    }
                }
            }), 422

        else:
            user = UserModel.query.filter(
                UserModel.passkey==passkey,
                UserModel.verification_code==str(data['signature']), 
                UserModel.passkey_expire >= int(time.time())).first()        

            if user is None:
                return jsonify({
                    'success': False,
                    'errors': {
                        'code': 422,
                        'messages': {
                            'passkey': ['Invalid passkey or has expired.']
                        }
                    }
                }), 422

            else:  

                user.passkey = None
                user.passkey_expire = None
                user.verification_code = None

              

                schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
                person = schema.dump(user).data

                return jsonify({
                    'success': True,
                    'message': '',
                    'data': {
                        'user': person,
                        'access_token': access_token,
                        'refresh_token': refresh_token
                    }  
                }), 200



    '''
    @method PUT
    @endpoint /v1/auth/verify
    @permission anonymous
    @return verification_code
    '''

    reverify_args = {
        'code': fields.Str(required=True, validate=[validate_required])
    }

    @cross_origin()
    @use_args(reverify_args, locations=['json'])
    def put(self, payload):
        
        ''' Reverify '''        
        
        code = payload['code']

        data = parse_verification_token(code)
        list_keys = ['signature', 'expire']

        if not any(p in list_keys for p in data):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'verification_code': ['Invalid verification code or has expired.']
                    }
                }
            }), 422

        else:

            user = UserModel.query.filter_by(verification_code=data['signature']).first()
            if user is None:
                return jsonify({
                    'success': False,
                    'errors': {
                        'code': 422,
                        'messages': {
                            'verification_code': ['Invalid verification code or has expired.']
                        }
                    }
                }), 422
            else:
                
                verification_code = user.verify()
                return jsonify({
                    'success': True,
                    'message': '',
                    'data': {
                        'verification_code': verification_code
                    }
                }), 200


api.add_resource(Verify, '/auth/verify')



class Device(Resource):

    '''
    @method PUT
    @endpoint /v1/auth/device
    @permission @jwt_required
    @return success message
    '''

    @cross_origin()
  
    @current_user()
    def put(self):

        ''' Update device id '''

        user = UserModel.query.get(g.current_user.id)
        nid = request.headers.get('nid')

        if not user.nid:
            user.nid = nid
            db.session.commit()

        elif user.nid != nid:
            user.nid = nid
            db.session.commit()

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'nid': user.nid
            }
        }), 200


api.add_resource(Device, '/auth/device')

