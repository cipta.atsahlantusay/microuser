# -*- coding: utf-8 -*-
import arrow
import bleach
import re

from flask import (
    g,
    current_app,
    jsonify
)

from flask_cors import cross_origin

from flask_restful import Resource
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import(
    use_args,
    use_kwargs,
    parser
)
from slugify import slugify
from sqlalchemy.sql.expression import func, and_, or_

from api import cache, db,  mail
from api.v1 import api

from api.v1.models.users import (
    UserModel,
    PermissionModel,
    RoleModel
)
from api.v1.schemas.users import(
    UserSchema,
    PermissionSchema,
    RoleSchema
)
from api.v1.utils import(
    reverse_id,
    normalize_phone_number,
    denormalize_phone_number
)
from api.v1.utils.pagination import Pagination
from api.v1.decorators import (
    permission_required,
    current_user
)

from api.v1.tasks import (
    send_celery_email_queue
)

class Users(Resource):
    '''
    @method GET
    @endpoint /v1/users
    @access login_required
    @permission ['user', 'viewall']
    '''

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields')

    get_args = {
        'q': fields.Str(missing=None),
        'status': fields.Str(validate=[
            validate_required,
            validate.OneOf(choices=['unverified', 'verified', 'banned'], labels=['Unverified', 'Verified', 'Banned'], error="Invalid status choice")
        ], missing=None),        
        'role': fields.Int(missing=None),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[10,25,50,100], error='Invalid perpage choice'), missing=10),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    
    @permission_required('user', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' ALL USERS '''
            
        q = args['q']
        status = args['status']
        role = reverse_id(args['role']) if args['role'] else None
        page = args['page']
        perpage = args['perpage']
        offset = (page - 1) * perpage

        query = UserModel.get_all(q, status, role, page, perpage)
        total = UserModel.count_all(q, status, role)

        schema = UserSchema(many=True, only=args['only'], exclude=args['exclude'])
        users = schema.dump(query).data


        pagination = Pagination('api.users', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'users': users,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/users
    @access login_required
    @permission ['user', 'viewall']
    '''

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Please fill all required fields')

    def validate_fullname(value):
        fullname_match = re.match(r'^([a-zA-Z0-9 ]{3,45})$', value)

        if fullname_match is None:
            raise ValidationError('Invalid full name')

    post_args = {
        'fullname': fields.Str(required=True, validate=[
            validate_required,
            validate_fullname,
        ]),
        'phone': fields.Str(required=True, validate=[validate_required]),
        'email': fields.Email(required=True, validate=[validate_required]),
        'status': fields.Str(validate=[
            validate.OneOf(choices=['unverified', 'verified', 'banned'], labels=['Unverified', 'Verified', 'Banned'], error="Invalid status choice")
        ], missing='unverified'),
        'role': fields.Int(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    
    @permission_required('user', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' CREATE USER '''

        fullname = bleach.clean(payload['fullname'], strip=True)
        phone = payload['phone']
        email = payload['email']
        role_id = reverse_id(payload['role'])
        status = payload['status']

        phone_match = re.search(r'\(?(?:\+628|628|08)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}', phone)        
        email_match = re.match(r'^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email)
	
        if not phone_match:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'phone': ['Invalid phone number']
                    }
                }
            }), 422


        if not email_match:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'phone': ['Invalid email address']
                    }
                }
            }), 422
        
        role = RoleModel.query.get(role_id)

        if not role:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'role': ['Role not found or already deleted']
                    }
                }
            }), 404
        
        phone = normalize_phone_number(phone)

        user_exist = UserModel.is_exist(None, email, phone)

        if user_exist:
            return jsonify({
                'success': True,
                'errors': {
                    'code': 422,
                    'messages': {
                        'user': ['User already exist']
                    }
                }
            }), 422

        user = UserModel()        
        user.fullname = fullname
        user.phone = phone
        user.email = email
        user.status = status
        user.role_id = role.id

        db.session.add(user)
        db.session.commit()

        schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
        newuser = schema.dump(user).data

        # send email to new user regarding new account activation

        send_celery_email_queue.delay({
            'subject': 'Aktivasi Akun Micro',
            'to': user.email,
            'template': 'emails/activation',
            'variables': {
                'fullname': user.fullname,
                'email': user.email,
                'phone': user.phone
            }
        })

        return jsonify({
            'success': True,
            'message': 'New user has been created successfully',
            'data': {
                'user': newuser
            }
        }), 201

     
api.add_resource(Users, '/users')




class User(Resource):
    '''
    @method GET
    @endpoint /v1/user/<int:user_id>
    @access login_required
    @permission ['user', 'viewown']
    '''

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Field is required')

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    
    @permission_required('user', 'viewother')
    @use_args(get_args, locations=['query'])
    def get(self, args, user_id):

        ''' VIEW USERS '''
        
        user_id = reverse_id(user_id)
        user = UserModel.get_by(id=user_id)

        if not user:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['User not found or has been deleted.']
                    }
                }                    
            }), 404
           

        schema = UserSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(user).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'user': result        
            }        
        }), 200


    '''
    @method PUT
    @endpoint /v1/users/<int:user_id>
    @access login_required
    @permission ['user', 'editother']
    '''

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Field is required')

    def validate_fullname(value):
        fullname_match = re.match(r'^([a-zA-Z0-9 ]{3,45})$', value)

        if fullname_match is None:
            raise ValidationError('Invalid full name')

    put_args = {
        'fullname': fields.Str(missing=None, validate=[
            validate_fullname,
        ]),
        'phone': fields.Str(missing=None),
        'email': fields.Email(missing=None),
        'status': fields.Str(validate=[
            validate.OneOf(choices=['unverified', 'verified', 'banned'], labels=['Unverified', 'Verified', 'Banned'], error="Invalid status choice")
        ], missing=None),
        'role': fields.Int(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    
    @permission_required('user', 'editother')
    @use_args(put_args, locations=['json'])
    def put(self, payload, user_id):

        ''' EDIT USER '''

        user_id = reverse_id(user_id)
        user = UserModel.get_by(id=user_id)

        if not user:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['User not found or has been deleted']
                    }
                }                
            }), 404


        fullname = bleach.clean(payload['fullname'], strip=True) if payload['fullname'] else None
        phone = payload['phone']
        email = payload['email']
        role_id = reverse_id(payload['role']) if payload['role'] else None
        status = payload['status']

        if phone:
            phone = normalize_phone_number(phone)     
            phone_match = re.search(r'\(?(?:\+628|628|08)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}', phone)        
            
            if not phone_match:
                return jsonify({
                    'success': False,
                    'errors': {
                        'code': 422,
                        'messages': {
                            'phone': ['Invalid phone number']
                        }
                    }
                }), 422
        
        if email:
            email_match = re.match(r'^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email)

            if not email_match:
                return jsonify({
                    'success': False,
                    'errors': {
                        'code': 422,
                        'messages': {
                            'phone': ['Invalid email address']
                        }
                    }
                }), 422
        
        if role_id:

            role = RoleModel.query.get(role_id)

            if not role:
                return jsonify({
                    'success': False,
                    'errors': {
                        'code': 404,
                        'messages': {
                            'role': ['Role not found or already deleted']
                        }
                    }
                }), 404
        
        if email or phone:

            user_exist = UserModel.is_exist(user.id, email, phone)
        
            if user_exist:
                return jsonify({
                    'success': True,
                    'errors': {
                        'code': 422,
                        'messages': {
                            'user': ['User already exist']
                        }
                    }   
                }), 422
        
        if fullname:
            user.fullname = fullname
        
        if phone:
            user.phone = phone
        
        if email:
            user.email = email
        
        if status:
            user.status = status
        
        if role_id:
            user.role_id = role.id

        db.session.commit()

        schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(user).data

        # send email to new user regarding new account activation

        send_celery_email_queue.delay({
            'subject': 'Update Akun Micro',
            'to': user.email,
            'template': 'emails/account',
            'variables': {
                'fullname': user.fullname,
                'email': user.email,
                'phone': user.phone
            }
        })

        return jsonify({
            'success': True,
            'message': 'User has been updated successfully',
            'data': {
                'user': result
            }
        }), 200


    
    '''
    @method DELETE
    @endpoint /v1/users/<int:user_id>
    @access login_required
    @permission ['user', 'deleteother']
    '''

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    
    @permission_required('user', 'deleteother')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, user_id):

        ''' DELETE USER '''

        user_id = reverse_id(user_id)
        user = UserModel.get_by(id=user_id)

        if not user:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['User not found or has been deleted']
                    }
                }                
            }), 404

        schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(user).data

        db.session.delete(user)
        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'user has been deleted successfully',
            'data': {
                'user': result
            }
        }), 200
    

api.add_resource(User, '/users/<int:user_id>')


class UserSearch(Resource):
    '''
    @method GET
    @endpoint /v1/users/search
    @access login_required
    @permission ['user', 'viewall']
    '''

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError('Please fill all required fields')

    get_args = {
        'q': fields.Str(required=True),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[10,25,50,100], error='Invalid perpage choice'), missing=10),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
 
    }

    @cross_origin()
    
    @permission_required('user', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' SEARCH USER '''

        q = args['q']
        page = args['page']
        perpage = args['perpage']
        
        users, total = UserModel.search(q, page, perpage)
        print(users, total)
        schema = UserSchema(many=True, only=args['only'], exclude=args['exclude'])
        result = schema.dump(users).data

        pagination = Pagination('api.usersearch', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'users': result,
                'pagination': pagination.paginate
            }
        }), 200


api.add_resource(UserSearch, '/users/search')


