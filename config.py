import os
import redis
from datetime import timedelta

basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    
    SERVER_NAME = 'api.microuser:3010'


    # FLASK
    DEBUG = False
    TESTING = False
    SQLALCHEMY_ECHO = False
    FLASK_COVERAGE = False

    ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
    MAX_CONTENT_LENGTH = 20 * 1024 * 1024

    ERROR_404_HELP = False

    # BLEACH
    BLEACH_ALLOWED_TAGS = set(['a', 'b', 'em', 'i', 'li', 'ol',
                               'strong', 'ul', 'h1', 'h2', 'h3', 'h4', 'h5', 'p'])
    BLEACH_ALLOWED_ATTRIBUTES = ['href', 'title', 'style', 'src']
    BLEACH_ALLOWED_STYLES = ['font-family', 'font-weight']
    BLEACH_STRIP_TAGS = True

    SECRET_KEY = "M<\xec\xa3[j\x04FYk\xac\xa9 \x1fV\x13f\x8c\xdfc\xcf\x15+|\xc6\xae\x11\xf1'x\x8f\x8d\xe4\xd32Z\x7f\x9a\xcaJ\x08\x1e?e\xa4B5I\xb3\xff z1gmMbw\xa9\xdf\xb6\xd9\x7f\xf5\xb7\x13j\x04^t\xee\xef\xe6\x81\x7f\xbb\x11\x1dU7\x0c\xad\xe0\xf4-F\xbc\xaf\x88\x88\x0e\x17$bK\x93H\x00\xa9O"

    # CACHE
    CACHE_TYPE = 'redis'
    CACHE_DEFAULT_TIMEOUT = 60
    CACHE_REDIS_URL = 'redis://:123@10.10.2.31:9002/0'
    CACHE_KEY_PREFIX = 'microuser_cache'

    # RATE LIMIT
    RATELIMIT_HEADERS_ENABLED = True
    RATELIMIT_STORAGE_URL = 'redis://:123@10.10.2.31:9002/0'
    RATELIMIT_KEY_PREFIX = 'microuser_ratelimit'

    # CELERY
    CELERY_BROKER_URL = 'redis://:123@10.10.2.31:9002/1'
    CELERY_RESULT_BACKEND = 'redis://:123@10.10.2.31:9002/1'
    CELERY_ACCEPT_CONTENT = ['json', 'pickle']

    CELERYBEAT_SCHEDULE = {
	'test-celery': {
	    'task': 'api.v1.tasks.say_hello',
		'schedule': 5.0
	}
    } 

    #SMTP
    ADMIN_EMAIL = 'mail@tes.com'
    MAIL_SERVER = 'smtp.tes.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = 'username'
    MAIL_PASSWORD = 'secret'
    MAIL_DEFAULT_SENDER = 'robot@micro.id'
    MAIL_ASCII_ATTACHMENTS = True
    MAIL_DEBUG = True

    # JWT
    JWT_TOKEN_LOCATION = ['headers']
    JWT_BLACKLIST_ENABLED = True
    JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(days=30)
    JWT_REFRESH_TOKEN_EXPIRES = timedelta(days=365)

    CORS_ENABLED = True
    CORS_ALLOW_HEADERS = ['Origin', 'Accept', 'Content-Type', 'Authorization', 'Content-Length', 'X-Requested-With']
    # CORS_RESOURCES = {r"/v1/*": {"origins": "*"}}
    CORS_METHODS = ['GET', 'POST', 'DELETE', 'PUT', 'OPTIONS']
    CORS_SUPPORTS_CREDENTIALS = True

    # NEXMO
    NEXMO_API_KEY = 'b824ced9'
    NEXMO_SECRET_KEY = '5e560391a01394e0'

    
    # KONG
    KONG_API_URL = 'https://kong.pusri.co.id/'

    @staticmethod
    def init_app(app):
        pass


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'postgresql://kong:Pusrijaya@10.10.2.30:5432/microuser'


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://kong:Pusrijaya@10.10.2.30:5432/microuser'


class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://kong:Pusrijaya@10.10.2.30:5432/microuser'


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
